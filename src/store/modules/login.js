const login = {
  namespaced: true,
  state: {
    Email: null,
  },
  getters: {
    getEmail: (state) => state.Email,
  },

  mutations: {
    setEmail: (state, payload) => {
      state.Email = payload;
    },
  },
  actions: {
    doLogin({ commit }, value) {
      if (value !== undefined) {
        localStorage.setItem('Email', value);
        commit('setEmail', { value });
      }
    },
  },
};

export default login;
